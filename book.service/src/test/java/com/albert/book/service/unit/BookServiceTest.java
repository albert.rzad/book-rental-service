package com.albert.book.service.unit;

import com.albert.book.service.entities.Book;
import com.albert.book.service.enums.BookCategory;
import com.albert.book.service.exceptions.BookNotFoundException;
import com.albert.book.service.repositories.BookRepository;
import com.albert.book.service.services.implementations.BookServiceImpl;
import com.albert.book.service.services.interfaces.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BookServiceTest {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService = new BookServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindByTitle() {
        String title = "Test Book";
        Book expectedBook = new Book();
        when(bookRepository.findByTitle(title)).thenReturn(expectedBook);

        Book result = bookService.findByTitle(title);

        assertEquals(expectedBook, result);
        verify(bookRepository, times(1)).findByTitle(title);
    }

    @Test
    public void testFindAllByCategory() {
        BookCategory category = BookCategory.FANTASY;
        List <Book> expectedBooks = Arrays.asList(new Book(), new Book());
        when(bookRepository.findAllByCategory(category.getStringValue())).thenReturn(expectedBooks);

        List <Book> result = bookService.findAllByCategory(category);

        assertEquals(expectedBooks, result);
        verify(bookRepository, times(1)).findAllByCategory(category.getStringValue());
    }

    @Test
    public void testFindAllByAuthor() {
        String author = "Test Author";
        List <Book> expectedBooks = Arrays.asList(new Book(), new Book());
        when(bookRepository.findAllByAuthor(author)).thenReturn(expectedBooks);

        List <Book> result = bookService.findAllByAuthor(author);

        assertEquals(expectedBooks, result);
        verify(bookRepository, times(1)).findAllByAuthor(author);
    }

    @Test
    public void testDeleteByTitle() {
        String title = "Test Book";

        bookService.deleteByTitle(title);

        verify(bookRepository, times(1)).deleteByTitle(title);
    }

    @Test
    public void testExist() {
        String title = "Test Book";
        when(bookRepository.findByTitle(title)).thenReturn(new Book());

        assertTrue(bookService.exist(title));
        verify(bookRepository, times(1)).findByTitle(title);
    }

    @Test
    public void testSave() {
        Book book = new Book();

        bookService.save(book);

        verify(bookRepository, times(1)).save(book);
    }

    @Test
    public void testUpdateTitle() {
        String oldTitle = "Old Title";
        String newTitle = "New Title";
        Book existingBook = new Book();
        existingBook.setTitle(oldTitle);

        when(bookRepository.findByTitle(oldTitle)).thenReturn(existingBook);

        assertDoesNotThrow(() -> bookService.updateTitle(oldTitle, newTitle));
        assertEquals(newTitle, existingBook.getTitle());
        verify(bookRepository, times(1)).save(existingBook);
    }

    @Test
    public void testUpdateTitleThrowsNotFoundException() {
        String oldTitle = "Nonexistent Title";
        String newTitle = "New Title";

        when(bookRepository.findByTitle(oldTitle)).thenReturn(null);

        assertThrows(BookNotFoundException.class, () -> bookService.updateTitle(oldTitle, newTitle));
        verify(bookRepository, never()).save(any());
    }
}
