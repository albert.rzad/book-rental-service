package com.albert.book.service.unit;

import com.albert.book.service.entities.User;
import com.albert.book.service.repositories.UserRepository;
import com.albert.book.service.services.implementations.UserServiceImpl;
import com.albert.book.service.services.interfaces.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindByEmail() {
        String email = "test@example.com";
        User expectedUser = new User();
        when(userRepository.findByEmail(email)).thenReturn(expectedUser);

        User result = userService.findByEmail(email);

        assertEquals(expectedUser, result);
        verify(userRepository, times(1)).findByEmail(email);
    }

    @Test
    public void testFindByUsername() {
        String username = "testuser";
        User expectedUser = new User();
        when(userRepository.findByUsername(username)).thenReturn(expectedUser);

        User result = userService.findByUsername(username);

        assertEquals(expectedUser, result);
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testFindAll() {
        List <User> expectedUsers = Arrays.asList(new User(), new User());
        when(userRepository.findAllBy()).thenReturn(expectedUsers);

        List <User> result = userService.findAll();

        assertEquals(expectedUsers, result);
        verify(userRepository, times(1)).findAllBy();
    }

    @Test
    public void testDeleteByUsername() {
        String username = "testuser";

        userService.deleteByUsername(username);

        verify(userRepository, times(1)).deleteByUsername(username);
    }

    @Test
    public void testDeleteByEmail() {
        String email = "test@example.com";

        userService.deleteByEmail(email);

        verify(userRepository, times(1)).deleteByEmail(email);
    }

    @Test
    public void testExist() {
        String username = "testuser";
        when(userRepository.findByUsername(username)).thenReturn(new User());

        assertTrue(userService.exist(username));
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testSave() {
        User user = new User();

        userService.save(user);

        verify(userRepository, times(1)).save(user);
    }
}
