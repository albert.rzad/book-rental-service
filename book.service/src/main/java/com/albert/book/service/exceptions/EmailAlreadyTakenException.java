package com.albert.book.service.exceptions;

public class EmailAlreadyTakenException extends RuntimeException{
    public EmailAlreadyTakenException(String errorMessage) {
        super(errorMessage);
    }
}
