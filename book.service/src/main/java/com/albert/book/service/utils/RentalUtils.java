package com.albert.book.service.utils;

import com.albert.book.service.exceptions.BadDatesException;
import jakarta.validation.ValidationException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RentalUtils {
    public static void validateRentalRequest(LocalDate endDate, String bookTitle, String userEmail) throws ValidationException {
        LocalDate startDate = LocalDate.now();

        if (!isValidBookTitle(bookTitle)) {
            throw new ValidationException("Book title is not valid");
        }

        if (!isValidEmail(userEmail)) {
            throw new ValidationException("Email is not valid");
        }

        if (!isSameFormat(endDate.toString())) {
            throw new BadDatesException("Wrong end date format.");
        }

        if (!endDate.isAfter(startDate)) {
            throw new BadDatesException("Start date should be before end date.");
        }
    }

    private static boolean isSameFormat(String date) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate parsedDate = LocalDate.parse(date, formatter);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isValidBookTitle(String bookTitle) {
        if (bookTitle == null) {
            return false;
        }
        if (bookTitle.length() > 50) {
            return false;
        }
        return bookTitle.matches("[a-zA-Z0-9 ]+");
    }

    public static boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        }

        if (email.length() > 320) {
            return false;
        }
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }
}

