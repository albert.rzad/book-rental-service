package com.albert.book.service.exceptions;

public class BadDatesException extends RuntimeException {
    public BadDatesException(String errorMessage) {
        super(errorMessage);
    }
}
