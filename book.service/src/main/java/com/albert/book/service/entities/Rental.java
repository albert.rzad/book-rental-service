package com.albert.book.service.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class Rental {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;

    @ManyToOne
    @JoinColumn(name = "book_id")
    Book book;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Rental(LocalDate startDate, LocalDate endDate, Book book, User user) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.book = book;
        this.user = user;
    }
}