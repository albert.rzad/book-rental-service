package com.albert.book.service.controllers;

import com.albert.book.service.entities.User;
import com.albert.book.service.exceptions.EmailAlreadyTakenException;
import com.albert.book.service.exceptions.UsernameAlreadyTakenException;
import com.albert.book.service.services.interfaces.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.albert.book.service.Constants.*;
import static com.albert.book.service.utils.UserUtils.validateUser;

@RestController
@RequestMapping("/auth")
@Tag(name = "Authorization", description = "Operations related to authorization")
public class AuthorizationController {
    @Autowired
    private UserService userService;

    @Operation(summary = "Create a new user", description = "Creates a new user with the provided details.")
    @PostMapping("/register")
    public ResponseEntity <String> createUser(
            @Parameter(description = "Username of the user") @RequestParam String username,
            @Parameter(description = "Email of the user") @RequestParam String email,
            @Parameter(description = "Password of the user") @RequestParam String password) {

        if (!validateUser(email, username, password)) {
            throw new ValidationException(INVALID_DATA);
        }

        if (userService.exist(username)) {
            throw new UsernameAlreadyTakenException(USERNAME_TAKEN);
        }

        if (userService.findByEmail(email) != null) {
            throw new EmailAlreadyTakenException(EMAIL_TAKEN);
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        User user = new User(username, email, hashedPassword);
        userService.save(user);

        return new ResponseEntity <>(USER_CREATED, HttpStatus.CREATED);
    }

    @Operation(summary = "User Login", description = "Login a user with the provided credentials.")
    @PostMapping("/login")
    public ResponseEntity <String> loginUser(@RequestParam String username, @RequestParam String password, HttpSession session) {
        User user = userService.findByUsername(username);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if (user != null && passwordEncoder.matches(password, user.getPassword())) {
            session.setAttribute("username", username);
            session.setAttribute("password", password);
            return new ResponseEntity <>("User logged in successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity <>("Invalid username or password", HttpStatus.UNAUTHORIZED);
        }
    }

    @Operation(summary = "User Logout", description = "Logout the current user.")
    @PostMapping("/logout")
    public ResponseEntity <String> logoutUser(HttpSession session) {
        session.invalidate();
        return new ResponseEntity <>("User logged out successfully", HttpStatus.OK);
    }
}
