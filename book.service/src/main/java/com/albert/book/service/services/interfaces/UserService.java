package com.albert.book.service.services.interfaces;

import com.albert.book.service.entities.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    User findByEmail(String email);

    User findByUsername(String username);

    List <User> findAll();

    void deleteByUsername(String username);

    void deleteByEmail(String email);

    void save(User user);

    boolean exist(String username);
}
