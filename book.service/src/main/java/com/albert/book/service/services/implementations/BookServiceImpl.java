package com.albert.book.service.services.implementations;

import com.albert.book.service.entities.Book;
import com.albert.book.service.enums.BookCategory;
import com.albert.book.service.exceptions.BookNotFoundException;
import com.albert.book.service.repositories.BookRepository;
import com.albert.book.service.services.interfaces.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book findByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    @Override
    public List <Book> findAllByCategory(BookCategory category) {
        return bookRepository.findAllByCategory(category.getStringValue());
    }

    @Override
    public List <Book> findAllByAuthor(String author) {
        return bookRepository.findAllByAuthor(author);
    }

    @Override
    public void deleteByTitle(String title) {
        bookRepository.deleteByTitle(title);
    }

    @Override
    public boolean exist(String title) {
        return bookRepository.findByTitle(title) != null;
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void updateTitle(String oldTitle, String newTitle) {
        Book bookToUpdate = bookRepository.findByTitle(oldTitle);

        if (bookToUpdate != null) {
            bookToUpdate.setTitle(newTitle);
            bookRepository.save(bookToUpdate);
        } else {
            throw new BookNotFoundException("Book with title " + oldTitle + " not found");
        }
    }
}
