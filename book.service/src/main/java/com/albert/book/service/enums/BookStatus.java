package com.albert.book.service.enums;

public enum BookStatus {
    AVAILABLE("Available"),
    UNAVAILABLE("Unavailable"),
    RENTED("Rented");

    private final String stringValue;

    BookStatus(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }
}

