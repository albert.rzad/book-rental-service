package com.albert.book.service.controllers;

import com.albert.book.service.entities.Book;
import com.albert.book.service.entities.Rental;
import com.albert.book.service.entities.User;
import com.albert.book.service.exceptions.BookNotFoundException;
import com.albert.book.service.exceptions.RentalNotFoundException;
import com.albert.book.service.exceptions.UserNotFoundException;
import com.albert.book.service.services.interfaces.BookService;
import com.albert.book.service.services.interfaces.RentalService;
import com.albert.book.service.services.interfaces.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.albert.book.service.Constants.*;
import static com.albert.book.service.session.SessionUtils.validateSession;
import static com.albert.book.service.utils.RentalUtils.validateRentalRequest;

@RestController
@RequestMapping("/rental")
@Tag(name = "Rentals", description = "Operations related to rentals")
public class RentalController {

    @Autowired
    private UserService userService;
    @Autowired
    private BookService bookService;
    @Autowired
    private RentalService rentalService;

    @Operation(summary = "Create a new rental", description = "Creates a new rental with the provided details.")
    @PostMapping("/create")
    public ResponseEntity <String> createRental(
            @RequestParam LocalDate endDate,
            @RequestParam String title,
            @RequestParam String email,
            HttpSession session) {
        validateSession(session, userService);
        try {
            validateRentalRequest(endDate, title, email);

            User user = userService.findByEmail(email);
            Book book = bookService.findByTitle(title);
            LocalDate startDate = LocalDate.now();

            if (user != null) {
                if (book != null) {
                    Rental rental = new Rental(startDate, endDate, book, user);
                    rentalService.save(rental);
                    return new ResponseEntity <>("Rental created successfully", HttpStatus.CREATED);
                }
                throw new BookNotFoundException(BOOK_NOT_FOUND);
            }
            throw new UserNotFoundException(USER_WITH_GIVEN_EMAIL_NOT_FOUND);

        } catch (ValidationException ve) {
            return new ResponseEntity <>(ve.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(summary = "Get a rental by ID", description = "Retrieves a rental with the specified ID.")
    @GetMapping("/byId")
    public ResponseEntity <Rental> getRental(@RequestParam Long id, HttpSession session) {
        validateSession(session, userService);
        Optional <Rental> rental = rentalService.findById(id);

        return rental.map(value -> new ResponseEntity <>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity <>(null, HttpStatus.NOT_FOUND));
    }

    @Operation(summary = "Get all rentals by user", description = "Retrieves all rentals associated with the specified user.")
    @GetMapping("/allByUser")
    public ResponseEntity <List <Rental>> getAllByUser(@RequestParam String username, HttpSession session) {
        validateSession(session, userService);
        User user = userService.findByUsername(username);
        List <Rental> rentals = rentalService.getAllByUser(user);
        return new ResponseEntity <>(rentals, HttpStatus.OK);
    }

    @Operation(summary = "Get all rentals", description = "Retrieves all rentals.")
    @GetMapping("/all")
    public List <Rental> getAll(HttpSession session) {
        validateSession(session, userService);
        return rentalService.getAll();
    }

    @Operation(summary = "Delete a rental by ID", description = "Deletes a rental with the specified ID.")
    @DeleteMapping("/byId")
    public ResponseEntity <String> deleteById(@RequestParam Long id, HttpSession session) {
        validateSession(session, userService);
        if (rentalService.exist(id)) {
            rentalService.deleteById(id);
            return new ResponseEntity <>("Rental with id: " + id + " successfully removed", HttpStatus.OK);
        }
        throw new RentalNotFoundException(RENTAL_NOT_FOUND);
    }

    @Operation(summary = "Update the end date of a rental", description = "Updates the end date of a rental with the specified details.")
    @PutMapping("/updateEndDate")
    public ResponseEntity <String> updateEndDate(@RequestParam Long id, @RequestParam LocalDate newEndDate, HttpSession session) {
        validateSession(session, userService);
        if (rentalService.exist(id)) {
            rentalService.updateEndDate(id, newEndDate);
            return new ResponseEntity <>("Rental with id: " + id + " updated successfully", HttpStatus.OK);
        } else {
            throw new RentalNotFoundException(RENTAL_NOT_FOUND);
        }
    }
}
