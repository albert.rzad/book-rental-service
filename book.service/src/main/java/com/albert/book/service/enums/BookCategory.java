package com.albert.book.service.enums;

public enum BookCategory {
    GRAPHIC_NOVEL("Graphic Novel"),
    FANTASY("Fantasy"),
    SCIENCE_FICTION("Science Fiction"),
    DRAMA("Drama"),
    ROMANCE("Romance"),
    MYSTERY("Mystery"),
    HISTORICAL("Historical"),
    BIOGRAPHY("Biography"),
    SCIENTIFIC("Scientific"),
    SELF_HELP("Self-Help");

    private final String stringValue;

    BookCategory(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }
}