package com.albert.book.service.utils;

import com.albert.book.service.enums.BookCategory;

public class BookUtils {

    public static boolean validCategory(String value) {
        try {
            Enum.valueOf(BookCategory.class, value);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
