package com.albert.book.service.services.interfaces;

import com.albert.book.service.entities.Book;
import com.albert.book.service.enums.BookCategory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {
    Book findByTitle(String title);

    List <Book> findAllByCategory(BookCategory category);

    List <Book> findAllByAuthor(String author);

    void deleteByTitle(String title);

    boolean exist(String title);

    void save(Book book);

    void updateTitle(String oldTitle, String newTitle);
}
