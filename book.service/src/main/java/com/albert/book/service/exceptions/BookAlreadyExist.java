package com.albert.book.service.exceptions;

public class BookAlreadyExist extends RuntimeException{
    public BookAlreadyExist(String errorMessage) {
        super(errorMessage);
    }
}
