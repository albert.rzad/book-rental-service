package com.albert.book.service.repositories;

import com.albert.book.service.entities.Rental;
import com.albert.book.service.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RentalRepository extends JpaRepository <Rental, Long> {
    Optional <Rental> findById(Long id);

    Rental findByUser(User user);

    List <Rental> getAllByUser(User user);

    void deleteById(Long id);
}
