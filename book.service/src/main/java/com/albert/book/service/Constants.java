package com.albert.book.service;

public class Constants {
    public static final String INVALID_DATA = "Provide correct data.";
    public static final String USERNAME_TAKEN = "Username has been already taken. Try another one";
    public static final String EMAIL_TAKEN = "Email has been already taken. Try another one";
    public static final String USER_CREATED = "User created successfully";
    public static final String BOOK_ALREADY_EXIST = "Book with this title already exists.";
    public static final String CATEGORY_ENUM_MESSAGE = "Given category does not fit category enum.";
    public static final String USER_WITH_GIVEN_EMAIL_NOT_FOUND = "User with given email not found.";
    public static final String BOOK_NOT_FOUND = "Book with given title not found.";
    public static final String RENTAL_NOT_FOUND = "Rental with given id not found.";
}
