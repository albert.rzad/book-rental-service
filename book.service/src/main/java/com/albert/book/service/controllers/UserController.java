package com.albert.book.service.controllers;

import com.albert.book.service.entities.User;
import com.albert.book.service.exceptions.UserNotFoundException;
import com.albert.book.service.services.interfaces.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.albert.book.service.session.SessionUtils.validateSession;

@RestController
@RequestMapping("/user")
@Tag(name = "Users", description = "Operations related to users")
public class UserController {
    @Autowired
    private UserService userService;

    @Operation(summary = "Get a user by email", description = "Retrieves a user with the specified email.")
    @GetMapping("/byEmail")
    public ResponseEntity <User> getUserByEmail(
            @Parameter(description = "Email of the user") @RequestParam String email, HttpSession session) {
        validateSession(session, userService);
        User user = userService.findByEmail(email);

        if (user != null) {
            return new ResponseEntity <>(user, HttpStatus.OK);
        }
        return new ResponseEntity <>(HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Get a user by username", description = "Retrieves a user with the specified username.")
    @GetMapping("/byUsername")
    public ResponseEntity <User> getUserByUsername(
            @Parameter(description = "Username of the user") @RequestParam String username, HttpSession session) {
        validateSession(session, userService);
        User user = userService.findByUsername(username);

        if (user != null) {
            return new ResponseEntity <>(user, HttpStatus.OK);
        }
        return new ResponseEntity <>(HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Get all users", description = "Retrieves all users.")
    @GetMapping("/all")
    public ResponseEntity <List <User>> getAllUsers(HttpSession session) {
        validateSession(session, userService);
        List <User> users = userService.findAll();
        return new ResponseEntity <>(users, HttpStatus.OK);
    }

    @Operation(summary = "Delete a user by email", description = "Deletes a user with the specified email.")
    @DeleteMapping("/byEmail")
    public ResponseEntity <String> deleteByEmail(
            @Parameter(description = "Email of the user") @RequestParam String email, HttpSession session) {
        validateSession(session, userService);
        if (userService.findByEmail(email) != null) {
            userService.deleteByEmail(email);
            return new ResponseEntity <>("User with email: " + email + " successfully removed", HttpStatus.OK);
        }
        throw new UserNotFoundException("User with email " + email + " not found.");
    }

    @Operation(summary = "Delete a user by username", description = "Deletes a user with the specified username.")
    @DeleteMapping("/byUsername")
    public ResponseEntity <String> deleteByUsername(
            @Parameter(description = "Username of the user") @RequestParam String username, HttpSession session) {
        validateSession(session, userService);
        if (userService.findByUsername(username) != null) {
            userService.deleteByUsername(username);
            return new ResponseEntity <>("User with username: " + username + " successfully removed", HttpStatus.OK);
        }
        throw new UserNotFoundException("User with username " + username + " not found.");
    }
}
