package com.albert.book.service.controllers;

import com.albert.book.service.entities.Book;
import com.albert.book.service.enums.BookCategory;
import com.albert.book.service.enums.BookStatus;
import com.albert.book.service.exceptions.BadEnumValueException;
import com.albert.book.service.exceptions.BookAlreadyExist;
import com.albert.book.service.exceptions.BookNotFoundException;
import com.albert.book.service.services.interfaces.BookService;
import com.albert.book.service.services.interfaces.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.albert.book.service.Constants.BOOK_ALREADY_EXIST;
import static com.albert.book.service.Constants.CATEGORY_ENUM_MESSAGE;
import static com.albert.book.service.session.SessionUtils.validateSession;
import static com.albert.book.service.utils.BookUtils.validCategory;

@RestController
@RequestMapping("/book")
@Tag(name = "Books", description = "Operations related to books")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Operation(summary = "Create a new book", description = "Creates a new book with the provided details.")
    @PostMapping("/create")
    public ResponseEntity <String> createBook(
            @Parameter(description = "Title of the book") @RequestParam String title,
            @Parameter(description = "Author of the book") @RequestParam String author,
            @Parameter(description = "Category of the book") @RequestParam String category,
            @Parameter(description = "Status of the book") @RequestParam String status,
            @Parameter(description = "Publisher of the book") @RequestParam String publisher,
            HttpSession session
    ) {
        validateSession(session, userService);
        try {
            BookCategory categoryEnum = BookCategory.valueOf(category.toUpperCase());
            BookStatus statusEnum = BookStatus.valueOf(status.toUpperCase());

            if (bookService.exist(title)) {
                throw new BookAlreadyExist(BOOK_ALREADY_EXIST);
            }

            Book book = new Book(title, author, categoryEnum, statusEnum, publisher);
            bookService.save(book);
        } catch (IllegalArgumentException ex) {
            return new ResponseEntity <>("Category or status do not fit enum values.", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity <>("Book created successfully.", HttpStatus.CREATED);
    }

    @Operation(summary = "Get a book by title", description = "Retrieves a book with the specified title.")
    @GetMapping("/byTitle")
    public ResponseEntity <Book> getBookByTitle(
            @Parameter(description = "Title of the book") @RequestParam String title, HttpSession session) {
        Book book = bookService.findByTitle(title);

        if (book != null) {
            return new ResponseEntity <>(book, HttpStatus.OK);
        }
        return new ResponseEntity <>(HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Get all books by author", description = "Retrieves all books written by the specified author.")
    @GetMapping("/allByAuthor")
    public ResponseEntity <List <Book>> getAllByAuthor(
            @Parameter(description = "Author of the books") @RequestParam String author, HttpSession session) {
        validateSession(session, userService);
        List <Book> books = bookService.findAllByAuthor(author);

        if (books.size() == 0) {
            return new ResponseEntity <>(books, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity <>(books, HttpStatus.OK);
    }

    @Operation(summary = "Get all books by category", description = "Retrieves all books in the specified category.")
    @GetMapping("/allByCategory")
    public ResponseEntity <List <Book>> getAllByCategory(
            @Parameter(description = "Category of the books") @RequestParam String category, HttpSession session) {
        validateSession(session, userService);
        if (validCategory(category)) {
            BookCategory categoryEnum = BookCategory.valueOf(category.toUpperCase());
            List <Book> books = bookService.findAllByCategory(categoryEnum);
            if (books.size() == 0) {
                return new ResponseEntity <>(books, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity <>(books, HttpStatus.OK);
        }
        throw new BadEnumValueException(CATEGORY_ENUM_MESSAGE);
    }

    @Operation(summary = "Delete a book by title", description = "Deletes a book with the specified title.")
    @DeleteMapping("/byTitle")
    public ResponseEntity <String> deleteByTitle(
            @Parameter(description = "Title of the book") @RequestParam String title, HttpSession session) {
        validateSession(session, userService);
        if (bookService.findByTitle(title) != null) {
            bookService.deleteByTitle(title);
            return new ResponseEntity <>("Book with title: " + title + " successfully removed", HttpStatus.OK);
        }
        throw new BookNotFoundException("Book with title " + title + " not found.");
    }

    @Operation(summary = "Update the title of a book", description = "Updates the title of a book with the specified details.")
    @PutMapping("/updateTitle")
    public ResponseEntity <String> updateTitle(
            @Parameter(description = "Old title of the book") @RequestParam String oldTitle,
            @Parameter(description = "New title of the book") @RequestParam String newTitle,
            HttpSession session) {
        validateSession(session, userService);
        bookService.updateTitle(oldTitle, newTitle);
        return new ResponseEntity <>("Book title successfully updated.", HttpStatus.OK);
    }
}
