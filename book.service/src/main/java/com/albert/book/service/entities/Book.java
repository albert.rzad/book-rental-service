package com.albert.book.service.entities;

import com.albert.book.service.enums.BookCategory;
import com.albert.book.service.enums.BookStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "books")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String author;
    private String category;
    private String status;
    private String publisher;

    public Book(String title, String author, BookCategory category, BookStatus status, String publisher) {
        this.title = title;
        this.author = author;
        this.category = category.getStringValue();
        this.status = status.getStringValue();
        this.publisher = publisher;
    }
}