package com.albert.book.service.repositories;

import com.albert.book.service.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository <Book, Long> {
    Book findByTitle(String title);

    List <Book> findAllByCategory(String category);

    List <Book> findAllByAuthor(String author);

    void deleteByTitle(String title);
}
