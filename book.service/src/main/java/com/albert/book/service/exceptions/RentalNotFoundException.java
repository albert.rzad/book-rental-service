package com.albert.book.service.exceptions;


public class RentalNotFoundException extends RuntimeException {
    public RentalNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}