package com.albert.book.service.exceptions;

public class UsernameAlreadyTakenException extends RuntimeException {
    public UsernameAlreadyTakenException(String errorMessage) {
        super(errorMessage);
    }
}
