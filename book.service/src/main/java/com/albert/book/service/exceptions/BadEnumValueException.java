package com.albert.book.service.exceptions;

public class BadEnumValueException extends RuntimeException{
    public BadEnumValueException(String errorMessage) {
        super(errorMessage);
    }
}
